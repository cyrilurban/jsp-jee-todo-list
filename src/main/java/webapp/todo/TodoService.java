package webapp.todo;

import webapp.todo.Todo;

import java.util.ArrayList;
import java.util.List;

public class TodoService {
    private static List<Todo> todos = new ArrayList();

    static {
        todos.add(new Todo("learn Spring"));
        todos.add(new Todo("learn Hibernate"));
        todos.add(new Todo("learn JEE"));
    }

    public static List<Todo> getTodos() {
        return todos;
    }

    public void addTodo(Todo todo) {
        this.todos.add(todo);
    }

    public void deleteTodo(int todoIndex) {
        this.todos.remove(todoIndex);
    }
}
