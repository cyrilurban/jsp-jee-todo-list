<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
   <head>
       <title>Todo list</title>
       <link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

       <style>
            .footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                height: 60px;
                background-color: #f5f5f5;
            }
       </style>
   </head>

    <body>
        <nav role="navigation" class="navbar navbar-default">
            <div class="">
                <a href="/" class="navbar-brand">${name} </a>
            </div>

            <div class="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"> <a href="#"> Home </a> </li>
                    <li> <a href="/todo.do">Todos </a> </li>
                    <li> <a href="https://www.linkedin.com/in/cyril-urban/"> Cyril Urban </a> </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li> <a href="/login.do"> Login </a> </li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <H1> Welcome ${name} </H1>

            Your Todos are
            <ol>
                <c:forEach items="${todos}" var="todo" varStatus="myIndex">
                    <li> ${todo.name} &nbsp &nbsp <a href="/todo-delete.do?id=${myIndex.index}"> Delete </a> </li>
                </c:forEach>
            </ol>

            <p>
                <font color="red">${errorMessage}</font>
            </p>
            <form method="POST" action="/todo.do">
                New Todo : <input name="todo" type="text" /> <input name="add" type="submit" />
            </form>
        </div>

        <footer class="footer">
            <div class="container">
            <br />
                <p> Urban Soft </p>
            </div>
        </footer>

        <script src="webjars/jquery/1.9.1/jquery.min.js"> </script>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"> </script>
    </body>
</html>